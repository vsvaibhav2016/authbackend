var express = require('express');
var router = express.Router();
var VerificationCode = require('./verificationcode.controller');

module.exports = function(router){
    router.post('/sendcode', VerificationCode.sendcode);
    router.post('/verifycode', VerificationCode.verifycode);
}