var passport = require('passport');
var local = require('passport-local');
const transporter = require('../../utils/mailer/email-config');
var localStrategy = local.Strategy;

var User = require('../../api/user/user.dao');
const properties = require('../properties');

var localOptions = {
    usernameField : 'email',
    passwordField : 'email',
    passReqToCallback : true
}

var localLoginOptions = {
    usernameField : 'email',
    passwordField : 'password',
    passReqToCallback : true
}



let randomFixedInteger = function (length) {
    return Math.floor(Math.pow(10, length-1) + Math.random() * (Math.pow(10, length) - Math.pow(10, length-1) - 1));
};



module.exports = {
    localSignup : new localStrategy(localOptions, function( req, email,password, done){
        console.log("email",email);
        User.getByEmail({
                    email : email
                },function(err, user){


                    if(err){
                        return done(err);
                    }
                    if(user){
                        return done(null, false);
                    }else{

                        randomCode = randomFixedInteger(6);

                        mailOptions = {
                            from : properties.senderEmail,
                            to : email,
                            subject : "Check Email Verification Code",
                            html : "<p>Hi <span style='color:cyan;font-weight:bolder;font-size:16px'>"+req.body.email+"</span>, please find your email verification code in this email<p> <br><div style='font-weight:bolder;font-size:42px;color:#000'>"+randomCode+"<div>"
                        };


                        transporter.transporter.sendMail(mailOptions, function(err, info){
                            if(err){
                                console.log(err);
                            }
                            // res.json({
                            //     message: "Email Verification code sends on your email"
                            // });
                        });

                        var data = { 
                            email : email, 
                            // password : password, 
                            username: req.body.username, 
                            fullname: req.body.fullname,
                             verificationToken : randomCode
                        };

                        console.log("signup data",data);
                
                        User.create(data, function(err, data){
                            if(err){
                                return done(err);
                            }

                        setTimeout(function(){
                            
                            var data1 = {
                                        verificationToken : null
                                    }
                           User.update({email : email}, data1, function(err, user){
                                if(err){
                                        response.send(err);
                                }
                            })
                        },180000);
                        return done(null, data);
                    })   
            }
        })
    }),


    localLogin : new localStrategy(localLoginOptions, function(req,email,password,done){
        User.getByEmail({
            email : email
        },function(err, user){
            console.log("user",user);
            if(err){
                return done(err);
            }

            if(!user){
                return done(null, false, {error : "User of that email id is not found"});
            }

            

            User.comparePassword(password, user.password, function(err, isMatch){
                console.log("password",password);
                console.log("user.password",user.password);
                if(err){
                    console.log("err",err);
                    return done(err)
                }

                if(!isMatch){
                    console.log("not match");
                    return done(null, false, {message : "Password is incorrect"})
                }else{
                    console.log("match");
                    return done(null, user)
                }
            })
            

        })
    })
}