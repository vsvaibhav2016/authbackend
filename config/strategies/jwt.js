var JWT = require('passport-jwt');
var JWTStrategy = JWT.Strategy;
var ExtractJWT = JWT.ExtractJwt;
var properties = require('../properties');
var User = require('../../api/user/user.dao');

var JWTOptions = {
    jwtFromRequest : ExtractJWT.fromAuthHeader(),
    secretOrKey : properties.secret
}

module.exports = new JWTStrategy(JWTOptions, function(payload, done){
    User.getById({_id : payload._id}, function(err, user){
        if(err){
            return done(err, false);
        }

        if(user){
            done(null, user)
        }else{
            done(null, false)
        }
    })
})