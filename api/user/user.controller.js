var jwt = require('jsonwebtoken');
var User = require('./user.dao');
var config = require('../../config/properties');



function generateToken(user){
    return jwt.sign(user, config.secret,{
        expiresIn : 10080
    });
}

function setUserInfo(request){
    return {
        _id : request._id,
        // email : request.email,
        // username : request.username,
        // fullname : request.fullname
    }
}


// Signup Controller Function

exports.signup = function(request, response, next){
    console.log("user signup");
    var user = setUserInfo(request.user);
    console.log("Signup user info",user);
    response.status(201).json({
        message : "Signup Successfully",
        token : 'JWT '+ generateToken(user),
        user : user
    })
};

// Login Controller Function

exports.login = function(request, response, next){
    var user = setUserInfo(request.user);

    response.status(200).json({
        message : "Login Successfully",
        token : 'JWT '+ generateToken(user),
        user : user
    })
};

 exports.profile = function(request, response, next){
    var user = setUserInfo(request.user);
    console.log("Signup user info",user);
    response.json({
        message : "Protected Routes",
        statusText : "authenticated user",
        user : user
    })
}; 










exports.updateUser = function(request, response, next){
    User.update({_id : request.params.id}, request.body, function(err, user){
        if(err){
            console.log("Error in updating user", err);
            response.send(err);
        }
        
        response.json(user);
    })
}



// Set password or Reset password 

exports.setPassword = function(request,response,next){
    /*consol.log("set password calling");
    consol.log("email", request.params);*/
    User.hashpassword(request.body.password, function(err, hash){
        if(err){
            next(err)
        }
        request.body.password = hash;
        User.update({email : request.params.email},request.body,function(err, user){
            if(err){
                console.log("Error in updating user", err);
                response.send(err);
            }

            if(!user){
                response.json({
                    message : "User not exist"
                })
            }

            response.json(user);
        })
    });
}

/* exports.resetpassword = function(request, response, next){
    request.body.password= User.hashPassword(request.body.password)
    User.update({email : request.params.email}, request.body, function(err, user){
        console.log("request.params",request.params.email);
        console.log(" request.body",request.body);
        
        if(err){
            console.log("Error in updating user", err);
            response.send(err);
        }
        
        response.json(user);
    })
} */

/* Change Password  or set password Functionality */
/*exports.changePassword = function(request, response, next){
    User.getById({_id : request.params.id}, function(err, user){
        if(err){
            response.json({
                text : false,
                message: err
            })
        }else{
            console.log(request.body);
            if(User.comparepassword(request.body.oldpassword, user.password)){
                var data = {
                            password : User.hashPassword(request.body.newpassword)
                        }

                        User.update({_id : user._id}, data, function(err, user1){
                            if(err){
                                console.log("Error in updating user", err);
                                response.send(err);
                            }
        
                        response.json(user1);
                    })
            }

            /* User.comparePassword(request.body.oldpassword, user.password, function(err, isMatch){
                 if(err){
                        response.json({
                            text: false,
                            message : err
                        });
                    }

                if(!isMatch){
                        response.json({
                            text :false,
                            message : "Password not matched"
                        });
                    }else{
                        console.log("req.body",request.body.newpassword);
                        var data = {
                            password : User.hashPassword(request.body.newpassword)
                        }


                        User.update({_id : user._id}, data, function(err, user1){
                            if(err){
                                console.log("Error in updating user", err);
                                response.send(err);
                            }
        
                        response.json(user1);
                    })




                    }

                    
            }) 
        }
    })
}*/