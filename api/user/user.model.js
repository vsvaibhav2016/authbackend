var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userSchema = new Schema({
    fullname : {
        type : String,
        unique : false
    },
    username : {
        type : String,
        unique : true,
    },
    email : {
        type : String,
        unique : true,
    },
    password :{
        type : String,
        unique : false
    },
    mobile : {
        type : Number,
        unique: true, 
        sparse: true,
        required : false
    },
    verified : {
        type : Boolean,
        default : false
    },
    verificationToken : {
       type : Number 
    }
},{timestamps : true});

module.exports = userSchema;